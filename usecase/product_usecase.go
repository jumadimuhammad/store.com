package usecase

import (
	"context"
	"fmt"
	_ "image/jpeg"
	_ "image/png"

	"store.com/entity/model"
	"store.com/entity/schema"
	"store.com/storage/order"
	"store.com/storage/product"
)

//go:generate mockery --name ProductUsecase --case snake --output ../mocks --disable-version-string

type (
	ProductUsecase interface {
		Insert(context.Context, model.ProductRequest) (*model.ProductResponse, error)
		Fetch(context.Context) ([]model.ProductResponse, error)
	}

	productUsecase struct {
		productStorage product.ProductStorage
		orderStorage   order.OrderStorage
	}
)

func NewProductUsecase(productStorage product.ProductStorage, orderStorage order.OrderStorage) ProductUsecase {
	return &productUsecase{
		productStorage: productStorage,
		orderStorage:   orderStorage,
	}
}

func (p *productUsecase) Insert(ctx context.Context, data model.ProductRequest) (*model.ProductResponse, error) {
	request := schema.Product{
		Name:  data.Name,
		Stock: data.Stock,
	}

	res, err := p.productStorage.Insert(ctx, request)
	if err != nil {
		return nil, err
	}

	product := model.ProductResponse{
		Id:    int32(res.Id),
		Name:  res.Name,
		Stock: data.Stock,
	}

	return &product, nil
}

func (p *productUsecase) Fetch(ctx context.Context) ([]model.ProductResponse, error) {
	res, err := p.productStorage.Fetch(ctx)
	if err != nil {
		return nil, err
	}

	results := []model.ProductResponse{}
	for i := range res {
		total, _ := p.orderStorage.GetByProductId(ctx, int32(res[i].Id))

		rest := model.ProductResponse{
			Id:        int32(res[i].Id),
			Name:      res[i].Name,
			Stock:     res[i].Stock - total,
			CreatedAt: fmt.Sprintf("%v", res[i].CreatedAt),
		}

		results = append(results, rest)
	}

	return results, nil
}
