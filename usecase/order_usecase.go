package usecase

import (
	"context"
	"errors"
	"fmt"

	"store.com/entity/model"
	"store.com/entity/schema"
	"store.com/storage/order"
	"store.com/storage/product"
)

//go:generate mockery --name OrderUsecase --case snake --output ../mocks --disable-version-string

type (
	OrderUsecase interface {
		Insert(context.Context, model.OrderRequest) (*model.OrderResponse, error)
		Fetch(context.Context) ([]model.OrderResponse, error)
		ValidationProduct(context.Context, int32, int32) error
	}

	orderUsecase struct {
		productStorage product.ProductStorage
		orderStorage   order.OrderStorage
	}
)

func NewOrderUsecase(orderStorage order.OrderStorage, productStorage product.ProductStorage) OrderUsecase {
	return &orderUsecase{
		productStorage: productStorage,
		orderStorage:   orderStorage,
	}
}

func (p *orderUsecase) ValidationProduct(ctx context.Context, id, total int32) error {
	totalSt, err := p.orderStorage.GetByProductId(ctx, id)
	if err != nil {
		return err
	}

	product, err := p.productStorage.GetById(ctx, id)
	if err != nil {
		return err
	}

	if product.Stock-totalSt < total {
		return errors.New(fmt.Sprintf("stok tersisa %v", product.Stock-totalSt))
	}

	return nil
}

func (p *orderUsecase) Insert(ctx context.Context, data model.OrderRequest) (*model.OrderResponse, error) {
	request := schema.Order{
		ProductId:   data.ProductId,
		ProductName: data.ProductName,
		UserId:      data.UserId,
		Total:       data.Total,
		Status:      data.Status,
	}

	res, err := p.orderStorage.Insert(ctx, request)
	if err != nil {
		return nil, err
	}

	order := model.OrderResponse{
		Id:        int32(res.Id),
		ProductId: res.ProductId,
		Product:   res.ProductName,
		Status:    res.Status,
		Total:     data.Total,
		CreatedAt: fmt.Sprintf("%v", res.CreatedAt),
	}

	return &order, nil
}

func (p *orderUsecase) Fetch(ctx context.Context) ([]model.OrderResponse, error) {
	res, err := p.orderStorage.Fetch(ctx)
	if err != nil {
		return nil, err
	}

	results := []model.OrderResponse{}
	for i := range res {
		rest := model.OrderResponse{
			Id:        int32(res[i].Id),
			ProductId: res[i].ProductId,
			Product:   res[i].ProductName,
			Total:     res[i].Total,
			Status:    res[i].Status,
			CreatedAt: fmt.Sprintf("%v", res[i].CreatedAt),
		}

		results = append(results, rest)
	}

	return results, nil
}
