package usecase

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"store.com/entity/model"
	"store.com/entity/schema"
	"store.com/mocks"
)

func TestValidationProduct(t *testing.T) {
	mockProductStorage := new(mocks.ProductStorage)
	mockOrderStorage := new(mocks.OrderStorage)
	ctx := context.Background()

	mockProductResponse := schema.Product{
		Name:  "Product 1",
		Stock: 5,
	}

	t.Run("success", func(t *testing.T) {
		mockProductStorage.On("GetById", mock.Anything, mock.AnythingOfType("int32")).Return(&mockProductResponse, nil)
		mockOrderStorage.On("GetByProductId", mock.Anything, mock.AnythingOfType("int32")).Return(int32(3), nil)

		u := NewOrderUsecase(mockOrderStorage, mockProductStorage)
		err := u.ValidationProduct(ctx, 1, 2)
		assert.Nil(t, err)
		assert.NoError(t, err)
		mockProductStorage.AssertExpectations(t)
		mockOrderStorage.AssertExpectations(t)
	})
}

func TestInsertOrder(t *testing.T) {
	mockProductStorage := new(mocks.ProductStorage)
	mockOrderStorage := new(mocks.OrderStorage)
	ctx := context.Background()
	mockOrderRequest := model.OrderRequest{
		ProductId:   1,
		ProductName: "Product 1",
		UserId:      1,
		Total:       3,
		Status:      "order",
	}

	mockOrderResponse := schema.Order{
		ProductId:   1,
		ProductName: "Product 1",
		UserId:      1,
		Total:       3,
		Status:      "order",
	}

	t.Run("success", func(t *testing.T) {
		mockOrderStorage.On("Insert", mock.Anything, mock.AnythingOfType("schema.Order")).Return(&mockOrderResponse, nil)

		u := NewOrderUsecase(mockOrderStorage, mockProductStorage)
		list, err := u.Insert(ctx, mockOrderRequest)

		assert.NotNil(t, list)
		assert.Nil(t, err)
		assert.NoError(t, err)
		mockProductStorage.AssertExpectations(t)
	})
}

func TestFetchOrder(t *testing.T) {
	mockProductStorage := new(mocks.ProductStorage)
	mockOrderStorage := new(mocks.OrderStorage)
	ctx := context.Background()

	mockProductResponse := []schema.Order{
		{
			ProductId:   1,
			ProductName: "Product 1",
			UserId:      1,
			Total:       3,
			Status:      "order",
		},
	}

	t.Run("success", func(t *testing.T) {
		mockOrderStorage.On("Fetch", mock.Anything).Return(mockProductResponse, nil)

		u := NewOrderUsecase(mockOrderStorage, mockProductStorage)
		list, err := u.Fetch(ctx)

		assert.NotNil(t, list)
		assert.Nil(t, err)
		assert.NoError(t, err)
		assert.Len(t, list, len(mockProductResponse))
		mockOrderStorage.AssertExpectations(t)
	})
}
