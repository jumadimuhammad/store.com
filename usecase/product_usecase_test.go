package usecase

import (
	"context"
	"testing"

	"store.com/entity/model"
	"store.com/entity/schema"
	"store.com/mocks"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestInsertProduct(t *testing.T) {
	mockProductStorage := new(mocks.ProductStorage)
	mockOrderStorage := new(mocks.OrderStorage)
	ctx := context.Background()
	mockProductRequest := model.ProductRequest{
		Name:  "Product 1",
		Stock: 10,
	}

	mockProductResponse := schema.Product{
		Name:  "Product 1",
		Stock: 10,
	}

	t.Run("success", func(t *testing.T) {
		mockProductStorage.On("Insert", mock.Anything, mock.AnythingOfType("schema.Product")).Return(&mockProductResponse, nil)

		u := NewProductUsecase(mockProductStorage, mockOrderStorage)
		list, err := u.Insert(ctx, mockProductRequest)

		assert.NotNil(t, list)
		assert.Nil(t, err)
		assert.NoError(t, err)
		mockProductStorage.AssertExpectations(t)
	})
}

func TestFetchProduct(t *testing.T) {
	mockProductStorage := new(mocks.ProductStorage)
	mockOrderStorage := new(mocks.OrderStorage)
	ctx := context.Background()

	mockProductList := []schema.Product{
		{
			Name:  "Product 1",
			Stock: 5,
		},
	}

	t.Run("success", func(t *testing.T) {
		mockProductStorage.On("Fetch", mock.Anything).Return(mockProductList, nil)
		mockOrderStorage.On("GetByProductId", mock.Anything, mock.AnythingOfType("int32")).Return(int32(3), nil)

		u := NewProductUsecase(mockProductStorage, mockOrderStorage)
		list, err := u.Fetch(ctx)

		assert.NotNil(t, list)
		assert.Nil(t, err)
		assert.NoError(t, err)
		assert.Len(t, list, len(mockProductList))
		mockProductStorage.AssertExpectations(t)
		mockOrderStorage.AssertExpectations(t)
	})
}
