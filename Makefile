mock:
	go generate ./usecase
	go generate ./storage/product
	go generate ./storage/order

test:
	go test ./handler -v -cover -covermode=atomic
	go test ./usecase -v -cover -covermode=atomic