## Kasus
    Tidak adanya validasi terkait jumlah persediaan barang yang tersisa sehingga ketika user melakukan order, data langsung bisa masuk dan di proses tanpa melihat ketersediaan barang yang ada.

## Solusi Kasus
    Solusi yang saya ajukan adalah melakukan validasi dari setiap order yg masuk, apabila order yang masuk melebihi barang yang tersedia maka akan di kembalikan response pemberitahuan jumlah sisa barang yang tersedia. Dengan cara seperti ini bisa mengatasi masalah tersebut dan membuat persedian barang tetap update.
## Solusi Aplikasi
---
## Menjalankan aplikasi `store.com`

1. Buat pengaturan db sebagai berikut :
    - db_name = store.com
    - db_host = localhost
    - d_user = root
    - db_password = ""
    - db_port = 3306

2. Jalankan program golang dengan perintah `go run .` atau `go run main.go`

## Testing Product
1. Insert 
    - endpoint : ``api/v1/products``
    - method : ``POST``
    - request : 
        ```json
        {
            "name":"Sepatu mahal seri baru",
	        "stock":5
        }
        ```
    - response :
        ```json
        {
            "status": 201,
            "message": "success",
            "data": {
                "id": 2,
                "name": "Sepatu mahal seri baru",
                "created_at": "2021-11-07 08:59:34 +0700 WIB"
            },
        }
        ```

2. GetAll 
    - endpoint : ``api/v1/products``
    - method : ``GET``
    - request : -
    - response :
        ```json
        {
            "status": 200,
            "message": "success",
            "data": [
                {
                "id": 1,
                "name": "Baju mahal baru",
                "stock": 2,
                "created_at": "2021-11-07 08:59:34 +0700 WIB"
                },
                {
                "id": 2,
                "name": "Sepatu mahal seri baru",
                "stock": 1,
                "created_at": "2021-11-07 08:59:42 +0700 WIB"
                }
            ]
        }
        ```

## Testing Order
1. Insert 
    - endpoint : ``api/v1/orders``
    - method : ``POST``
    - request : 
        ```json
        {
            "product_id":1,
            "product_name":"Baju mahal baru",
            "user_id":1,
            "total":3,
            "status":"order"
        }
        ```
    - response :
        ```json
        {
            "status": 201,
            "message": "success",
            "data": {
                "id": 1,
                "product_id": 1,
                "product": "Baju mahal baru",
                "status": "order",
                "total": 3,
                "created_at": "0001-01-01 00:00:00 +0000 UTC"
            }
        }
        ```

2. GetAll 
    - endpoint : ``api/v1/orders``
    - method : ``GET``
    - request : -
    - response :
        ```json
        {
            "status": 200,
            "message": "success",
            "data": [
                {
                    "id": 1,
                    "product_id": 1,
                    "product": "Baju mahal baru",
                    "status": "order",
                    "total": 3,
                    "created_at": "0001-01-01 11:00:00 +0000 UTC"
                },
                {
                    "id": 1,
                    "product_id": 2,
                    "product": "Sepatu mahal seri baru",
                    "status": "order",
                    "total": 4,
                    "created_at": "0001-01-01 03:00:00 +0000 UTC"
                }
            ]
        }
        ```
---
## Testing
    make test

