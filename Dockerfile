FROM golang:alpine

RUN apk update && apk add --no-cache git

WORKDIR /app

# Copy the source from the current directory to the working Directory inside the container 
COPY . .

RUN go mod tidy

# Build the Go app
RUN go build -o /main

# Expose port 8080 to the outside world
EXPOSE 8080

#Command to run the executable
CMD ["/main"]