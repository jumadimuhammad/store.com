module store.com

go 1.16

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/go-playground/validator/v10 v10.9.0
	github.com/joho/godotenv v1.4.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	gorm.io/driver/mysql v1.1.3
	gorm.io/gorm v1.22.2
)
