package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type configuration struct {
	dbHost string
	dbPort string
	dbUser string
	dbPass string
	dbName string
}

var ENV configuration

func LoadENV() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	mapENV()
}

func mapENV() {
	ENV.dbHost = os.Getenv("DB_HOST")
	ENV.dbPort = os.Getenv("DB_PORT")
	ENV.dbUser = os.Getenv("DB_USER")
	ENV.dbPass = os.Getenv("DB_PASS")
	ENV.dbName = os.Getenv("DB_NAME")
}
