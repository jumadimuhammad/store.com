package config

import (
	"fmt"
	"log"
	"net/url"

	"store.com/entity/schema"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func NewConnection() *gorm.DB {
	dbHost := ENV.dbHost
	dbPort := ENV.dbPort
	dbUser := ENV.dbUser
	dbPass := ENV.dbPass
	dbName := ENV.dbName
	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPass, dbHost, dbPort, dbName)

	val := url.Values{}
	val.Add("parseTime", "1")
	val.Add("loc", "Asia/Jakarta")
	dsn := fmt.Sprintf("%s?%s", connection, val.Encode())

	dbConn, err := gorm.Open(mysql.Open(dsn))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("RUN Automigrate.........")

	dbConn.AutoMigrate(
		&schema.Product{},
		&schema.Order{},
	)

	fmt.Println("Automigrate Successfully")

	return dbConn
}
