package app

import (
	"fmt"

	"store.com/config"
	"store.com/handler"
	orderStorage "store.com/storage/order"
	productStorage "store.com/storage/product"
	"store.com/usecase"

	"github.com/gin-gonic/gin"
)

func NewApp() *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	engine := gin.New()

	engine.GET("/health", func(c *gin.Context) {
		c.JSON(200, gin.H{"status": 200, "message": "All good"})
	})

	//Connection DB
	db := config.NewConnection()

	//Storage
	productStorage := productStorage.NewProductStorage(db)
	orderStorage := orderStorage.NewOrderStorage(db)

	//UseCase
	productUsecase := usecase.NewProductUsecase(productStorage, orderStorage)
	orderUsecase := usecase.NewOrderUsecase(orderStorage, productStorage)

	//Handler
	productHandler := handler.NewProductHandler(productUsecase)
	orderHandler := handler.NewOrderHandler(orderUsecase)

	//Router
	productHandler.Route(engine)
	orderHandler.Route(engine)

	fmt.Println("Running on port : 8080")
	return engine
}
