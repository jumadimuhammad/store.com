package main

import (
	"log"

	"store.com/app"
	"store.com/config"
)

func init() {
	config.LoadENV()
}

func main() {
	log.Fatal(app.NewApp().Run(":8080"))
}
