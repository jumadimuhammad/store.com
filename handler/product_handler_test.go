package handler

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"store.com/entity/model"
	"store.com/mocks"
	"store.com/utils"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestInsertProduct(t *testing.T) {
	mockProductUsecase := new(mocks.ProductUsecase)

	t.Run("success", func(t *testing.T) {
		mockRequest := model.ProductRequest{
			Name:  "product 1",
			Stock: 3,
		}

		mockResponse := model.ProductResponse{
			Id:        1,
			Name:      "product 1",
			Stock:     3,
			CreatedAt: "2021-11-05 15:52:59 +0700 WIB",
		}

		mockProductUsecase.On("Insert", mock.Anything, mock.AnythingOfType("model.ProductRequest")).Return(&mockResponse, nil)

		body, err := json.Marshal(mockRequest)
		assert.Nil(t, err)

		httpReq, err := http.NewRequest(http.MethodPost, "/api/v1/products", bytes.NewReader(body))
		assert.Nil(t, err)

		r := gin.Default()
		rr := httptest.NewRecorder()

		h := NewProductHandler(mockProductUsecase)

		r.POST("/api/v1/products", h.Insert)
		r.ServeHTTP(rr, httpReq)

		var resp utils.Respond
		err = json.Unmarshal(rr.Body.Bytes(), &resp)
		assert.Nil(t, err)
		assert.NotNil(t, resp)
		assert.EqualValues(t, http.StatusCreated, rr.Code)
		assert.EqualValues(t, 201, resp.Status)
		assert.EqualValues(t, "success", resp.Message)
		mockProductUsecase.AssertExpectations(t)
	})
}
func TestGetAllProduct(t *testing.T) {
	mockProductUsecase := new(mocks.ProductUsecase)

	t.Run("success", func(t *testing.T) {
		mockProductResponse := []model.ProductResponse{
			{
				Id:        1,
				Name:      "product 1",
				Stock:     3,
				CreatedAt: "2021-11-05 15:52:59 +0700 WIB",
			},
		}

		mockProductUsecase.On("Fetch", mock.Anything).Return(mockProductResponse, nil)

		httpReq, err := http.NewRequest(http.MethodGet, "/api/v1/products", nil)
		assert.Nil(t, err)

		r := gin.Default()
		rr := httptest.NewRecorder()

		h := NewProductHandler(mockProductUsecase)

		r.GET("/api/v1/products", h.GetAll)
		r.ServeHTTP(rr, httpReq)

		var resp utils.Respond
		err = json.Unmarshal(rr.Body.Bytes(), &resp)
		assert.Nil(t, err)
		assert.NotNil(t, resp)
		assert.EqualValues(t, http.StatusOK, rr.Code)
		assert.EqualValues(t, 200, resp.Status)
		assert.EqualValues(t, "success", resp.Message)
		mockProductUsecase.AssertExpectations(t)
	})
}
