package handler

import (
	"store.com/entity/model"
	"store.com/usecase"
	"store.com/utils"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type OrderHandler struct {
	orderUsecase usecase.OrderUsecase
}

func NewOrderHandler(orderUsecase usecase.OrderUsecase) *OrderHandler {
	return &OrderHandler{
		orderUsecase: orderUsecase,
	}
}

func (h *OrderHandler) Route(app *gin.Engine) {
	v1 := app.Group("api/v1")
	{
		v1.POST("/orders", h.Insert)
		v1.GET("/orders", h.GetAll)
	}
}

func (h *OrderHandler) Insert(c *gin.Context) {
	ctx := c.Request.Context()
	validate = validator.New()
	order := model.OrderRequest{}

	err := c.ShouldBindJSON(&order)
	if err != nil {
		utils.Response(c, 400, err.Error(), nil)
		return
	}

	err = validate.Struct(order)
	if err != nil {
		utils.Response(c, 400, err.Error(), nil)
		return
	}

	err = h.orderUsecase.ValidationProduct(ctx, order.ProductId, order.Total)
	if err != nil {
		if err.Error() == "record not found" {
			utils.Response(c, 404, "product tidak terdaftar", nil)
			return
		}

		utils.Response(c, 400, err.Error(), nil)
		return
	}

	res, err := h.orderUsecase.Insert(ctx, order)
	if err != nil {
		if err.Error() == "data not found" {
			utils.Response(c, 404, err.Error(), nil)
			return
		}

		utils.Response(c, 500, "server under maintenance", nil)
		return
	}

	utils.Response(c, 201, "success", res)
}

func (h *OrderHandler) GetAll(c *gin.Context) {
	ctx := c.Request.Context()
	res, err := h.orderUsecase.Fetch(ctx)
	if err != nil {
		if err.Error() == "data not found" {
			utils.Response(c, 404, err.Error(), nil)
			return
		}

		utils.Response(c, 500, "server under maintenance", nil)
		return
	}

	utils.Response(c, 200, "success", res)
}
