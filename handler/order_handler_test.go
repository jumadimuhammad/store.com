package handler

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"store.com/entity/model"
	"store.com/mocks"
	"store.com/utils"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestInsertOrder(t *testing.T) {
	mockOrderUsecase := new(mocks.OrderUsecase)

	t.Run("success", func(t *testing.T) {
		mockRequest := model.OrderRequest{
			ProductId:   1,
			ProductName: "Product 1",
			UserId:      1,
			Total:       3,
			Status:      "order",
		}

		mockResponse := model.OrderResponse{
			Id:        1,
			ProductId: 1,
			Product:   "Product 1",
			Total:     3,
			Status:    "order",
			CreatedAt: "2021-11-05 15:52:59 +0700 WIB",
		}

		mockOrderUsecase.On("Insert", mock.Anything, mock.AnythingOfType("model.OrderRequest")).Return(&mockResponse, nil)
		mockOrderUsecase.On("ValidationProduct", mock.Anything, mock.AnythingOfType("int32"), mock.AnythingOfType("int32")).Return(nil)

		body, err := json.Marshal(mockRequest)
		assert.Nil(t, err)

		httpReq, err := http.NewRequest(http.MethodPost, "/api/v1/orders", bytes.NewReader(body))
		assert.Nil(t, err)

		r := gin.Default()
		rr := httptest.NewRecorder()

		h := NewOrderHandler(mockOrderUsecase)

		r.POST("/api/v1/orders", h.Insert)
		r.ServeHTTP(rr, httpReq)

		var resp utils.Respond
		err = json.Unmarshal(rr.Body.Bytes(), &resp)
		assert.Nil(t, err)
		assert.NotNil(t, resp)
		assert.EqualValues(t, http.StatusCreated, rr.Code)
		assert.EqualValues(t, 201, resp.Status)
		assert.EqualValues(t, "success", resp.Message)
		mockOrderUsecase.AssertExpectations(t)
	})
}
func TestGetAllOrder(t *testing.T) {
	mockOrderUsecase := new(mocks.OrderUsecase)

	t.Run("success", func(t *testing.T) {
		mockOrderResponse := []model.OrderResponse{
			{
				Id:        1,
				ProductId: 1,
				Product:   "Product 1",
				Total:     3,
				Status:    "order",
				CreatedAt: "2021-11-05 15:52:59 +0700 WIB",
			},
		}

		mockOrderUsecase.On("Fetch", mock.Anything).Return(mockOrderResponse, nil)

		httpReq, err := http.NewRequest(http.MethodGet, "/api/v1/orders", nil)
		assert.Nil(t, err)

		r := gin.Default()
		rr := httptest.NewRecorder()

		h := NewOrderHandler(mockOrderUsecase)

		r.GET("/api/v1/orders", h.GetAll)
		r.ServeHTTP(rr, httpReq)

		var resp utils.Respond
		err = json.Unmarshal(rr.Body.Bytes(), &resp)
		assert.Nil(t, err)
		assert.NotNil(t, resp)
		assert.EqualValues(t, http.StatusOK, rr.Code)
		assert.EqualValues(t, 200, resp.Status)
		assert.EqualValues(t, "success", resp.Message)
		mockOrderUsecase.AssertExpectations(t)
	})
}
