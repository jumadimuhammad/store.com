package handler

import (
	"store.com/entity/model"
	"store.com/usecase"
	"store.com/utils"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

type ProductHandler struct {
	productUsecase usecase.ProductUsecase
}

func NewProductHandler(productUsecase usecase.ProductUsecase) *ProductHandler {
	return &ProductHandler{
		productUsecase: productUsecase,
	}
}

func (h *ProductHandler) Route(app *gin.Engine) {
	v1 := app.Group("api/v1")
	{
		v1.POST("/products", h.Insert)
		v1.GET("/products", h.GetAll)
	}
}

func (h *ProductHandler) Insert(c *gin.Context) {
	validate = validator.New()
	product := model.ProductRequest{}

	err := c.ShouldBindJSON(&product)
	if err != nil {
		utils.Response(c, 400, err.Error(), nil)
		return
	}

	err = validate.Struct(product)
	if err != nil {
		utils.Response(c, 400, err.Error(), nil)
		return
	}

	ctx := c.Request.Context()
	res, err := h.productUsecase.Insert(ctx, product)
	if err != nil {
		utils.Response(c, 500, "server under maintenance", nil)
		return
	}

	utils.Response(c, 201, "success", res)
}

func (h *ProductHandler) GetAll(c *gin.Context) {
	ctx := c.Request.Context()
	res, err := h.productUsecase.Fetch(ctx)
	if err != nil {
		if err.Error() == "data not found" {
			utils.Response(c, 404, err.Error(), nil)
			return
		}
		utils.Response(c, 500, "server under maintenance", nil)
		return
	}

	utils.Response(c, 200, "success", res)
}
