package model

type (
	OrderRequest struct {
		ProductId   int32    `json:"product_id" validate:"required"`
		ProductName string `json:"product_name" validate:"required"`
		UserId      int32    `json:"user_id" validate:"required"`
		Total       int32    `json:"total" validate:"required"`
		Status      string `json:"status" validate:"required"`
	}

	OrderResponse struct {
		Id        int32    `json:"id"`
		ProductId int32    `json:"product_id"`
		Product   string `json:"product"`
		Status    string `json:"status"`
		Total     int32    `json:"total"`
		CreatedAt string `json:"created_at"`
	}
)
