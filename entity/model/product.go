package model

type (
	ProductRequest struct {
		Name  string `json:"name" validate:"required"`
		Stock int32  `json:"stock" validate:"required"`
	}

	ProductResponse struct {
		Id        int32  `json:"id"`
		Name      string `json:"name"`
		Stock     int32  `json:"stock"`
		CreatedAt string `json:"created_at,omitempty"`
	}
)
