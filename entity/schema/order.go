package schema

type Order struct {
	Base
	ProductId   int32  `gorm:"type:int(11);notNull"`
	ProductName string `gorm:"type:varchar(255);notNull"`
	UserId      int32  `gorm:"type:int(11);notNull"`
	Total       int32  `gorm:"type:int(11);notNull"`
	Status      string `gorm:"type:varchar(11);notNull"`
}

func (Order) TableName() string {
	return "orders"
}
