package schema

type Product struct {
	Base
	Name  string `gorm:"type:varchar(255);notNull"`
	Stock int32  `gorm:"type:int(11);notNull"`
}

func (Product) TableName() string {
	return "products"
}
