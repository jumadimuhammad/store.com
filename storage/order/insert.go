package order

import (
	"context"

	"store.com/entity/schema"
)

func (m *orderStorage) Insert(ctx context.Context, data schema.Order) (*schema.Order, error) {
	result := m.Conn.WithContext(ctx).Create(&data)
	if result.Error != nil {
		return nil, result.Error
	}

	return &data, nil
}
