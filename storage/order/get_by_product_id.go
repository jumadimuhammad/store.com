package order

import (
	"context"
)

func (m *orderStorage) GetByProductId(ctx context.Context, id int32) (int32, error) {
	var count int32
	row := m.Conn.Table("orders").Where("product_id = ?", id).Select("sum(total)").Row()
	row.Scan(&count)
	if row.Err() != nil {
		return 0, row.Err()
	}

	return count, nil
}
