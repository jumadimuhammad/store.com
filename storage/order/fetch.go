package order

import (
	"context"
	"errors"

	"store.com/entity/schema"
)

func (m *orderStorage) Fetch(ctx context.Context) ([]schema.Order, error) {
	orders := []schema.Order{}
	result := m.Conn.Find(&orders)
	if result.Error != nil {
		return nil, result.Error
	}

	if len(orders) == 0 {
		return nil, errors.New("data not found")
	}
	return orders, nil
}
