package order

import (
	"context"

	"store.com/entity/schema"

	"gorm.io/gorm"
)

//go:generate mockery --name OrderStorage --case snake --output ../../mocks --disable-version-string

type (
	OrderStorage interface {
		Insert(context.Context, schema.Order) (*schema.Order, error)
		Fetch(context.Context) ([]schema.Order, error)
		GetByProductId(context.Context, int32) (int32, error)
	}

	orderStorage struct {
		Conn *gorm.DB
	}
)

func NewOrderStorage(dbCon *gorm.DB) OrderStorage {
	return &orderStorage{
		Conn: dbCon,
	}
}
