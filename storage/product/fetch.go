package product

import (
	"context"
	"errors"

	"store.com/entity/schema"
)

func (m *productStorage) Fetch(ctx context.Context, ) ([]schema.Product, error) {
	products := []schema.Product{}
	result := m.Conn.Find(&products)
	if result.Error != nil {
		return nil, result.Error
	}

	if len(products) == 0 {
		return nil, errors.New("data not found")
	}
	return products, nil
}