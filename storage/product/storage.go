package product

import (
	"context"

	"store.com/entity/schema"

	"gorm.io/gorm"
)

//go:generate mockery --name ProductStorage --case snake --output ../../mocks --disable-version-string

type (
	ProductStorage interface {
		Insert(context.Context, schema.Product) (*schema.Product, error)
		Fetch(context.Context) ([]schema.Product, error)
		GetById(context.Context, int32) (*schema.Product, error)
	}

	productStorage struct {
		Conn *gorm.DB
	}
)

func NewProductStorage(dbCon *gorm.DB) ProductStorage {
	return &productStorage{
		Conn: dbCon,
	}
}
