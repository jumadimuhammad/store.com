package product

import (
	"context"

	"store.com/entity/schema"
)

func (m *productStorage) Insert(ctx context.Context, data schema.Product) (*schema.Product, error) {
	result := m.Conn.WithContext(ctx).Create(&data)
	if result.Error != nil {
		return nil, result.Error
	}

	return &data, nil
}
