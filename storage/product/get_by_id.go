package product

import (
	"context"

	"store.com/entity/schema"
)

func (m *productStorage) GetById(ctx context.Context, id int32) (*schema.Product, error) {
	product := schema.Product{}
	result := m.Conn.First(&product, id)
	if result.Error != nil {
		return nil, result.Error
	}

	return &product, nil
}
